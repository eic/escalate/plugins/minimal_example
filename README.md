


```python
from pyjano.jana import Jana
from pyjano.jana.user_plugin import PluginFromSource

my_plugin = PluginFromSource('/home/romanov/eic/minimal_example', name='minimal_example')

jana = Jana()
jana.plugin('lund_reader') \
    .plugin('jana', nevents=1000, output='mydiHadSmearedOut.root') \
    .plugin(my_plugin, verbose=2) \
    .source('/home/romanov/Downloads/pipi.lund')\
    .run()
```

```bash
ejana 
-Pplugins=MiniPlugin,hepmc_reader,open_charm
-Popen_charm:verbose=0
-Popen_charm:e_beam_energy=5
-Popen_charm:ion_beam_energy=100
-Pjana:DEBUG_PLUGIN_LOADING=1
<source >.hepmc

```
