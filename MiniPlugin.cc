
#include <JANA/JEventProcessor.h>
#include <JANA/Services/JGlobalRootLock.h>
#include <TH1D.h>
#include <TFile.h>
#include <Math/Vector4D.h>

#include <ejana/MainOutputRootFile.h>
#include <MinimalistModel/McGeneratedParticle.h>
#include <fmt/core.h>


class MiniPluginProcessor: public JEventProcessor {
private:
    std::shared_ptr<ej::MainOutputRootFile> m_file;
    int m_verbose;                                      /// verbosity level. 0-none, 1-some, 2-all
    TDirectory* dest_dir; // Virtual subfolder inside dest_file used for this specific processor

    /// Declare histograms here
    TH1D* h1d_pt;



public:
    explicit MiniPluginProcessor() {
    }
    
    void Init() override {
        auto app = GetApplication();
        m_file = app->GetService<ej::MainOutputRootFile>();

        /// Set parameters to control which JFactories you use
        m_verbose = 1;
        app->SetDefaultParameter("minimal_example:verbose", m_verbose);

        if(m_verbose) {
            fmt::print("Initialization is called 1 time at jana start");
        }

        /// Set up histograms
        dest_dir = m_file->mkdir("minimal_example"); // Create a subdir inside dest_file for these results
        dest_dir->cd();
        h1d_pt = new TH1D("e_pt", "electron pt", 100,0,10);
        m_file->mkdir("another_dir")->cd(); // Create a subdir inside dest_file for these results
    }

    void Process(const std::shared_ptr<const JEvent>& event) override {

        /// Acquire any results you need for your analysis
        auto particles = event->Get<minimodel::McGeneratedParticle>();


        // Go through particles
        for(auto& particle: particles) {

            // select electron
            if( std::abs(particle->pdg) == 11 && particle->charge < 0) {
                ROOT::Math::PxPyPzMVector p(particle->px, particle->py, particle->pz, particle->m);
                h1d_pt->Fill(p.pt());

                if(m_verbose == 2) {
                    fmt::print("e pt = {}  theta = {} \n", p.pt(), p.theta()*180/3.1415);
                }
            }
        }
    }

    void Finish() override {
        fmt::print("MesonStructureProcessor::Finish(). Cleanup\n");
    }
};
    
extern "C" {
    void InitPlugin(JApplication *app) {
        InitJANAPlugin(app);
        app->Add(new MiniPluginProcessor);
    }
}
    
